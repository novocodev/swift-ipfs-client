import XCTest

import SwiftIpfsApiTests

var tests = [XCTestCaseEntry]()
tests += SwiftIpfsApiTests.allTests()
XCTMain(tests)
